Installing Latest SWIG (> SWIG 3.0.8)
==================================================

sudo apt-get remove swig
sudo apt-get install automake
sudo apt-get install byacc

git clone https://github.com/swig/swig.git
cd swig
./autogen.sh
./configure
make -j4
sudo make install
sudo ldconfig


On PCRE Error
-------------------------------

# At swig root,

wget https://ftp.pcre.org/pub/pcre/pcre-8.43.tar.bz2
bzip2 -d pcre-8.43.tar.bz2
./Tools/pcre-build.sh 


Installing Snowboy
=================================

sudo apt-get install libatlas-base-dev

git clone https://github.com/kitt-ai/snowboy
cd swig/Python
make
cp snowboydetect.py _snowboydetect.so ~/rs4/rs4/apis/snowboy
cp -r resources ~/rs4/rs4/apis/snowboy/
cp ../../examples/Python3/*.* ~/rs4/rs4/apis/snowboy/
