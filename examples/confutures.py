import rs4

def is_prime(n):
    if n < 2:
        return False
    if n is 2 or n is 3:
        return True
    if n % 2 is 0 or n % 3 is 0:
        return False
    if n < 9:
        return True
    k, l = 5, n ** 0.5
    while k <= l:
        if n % k is 0 or n % (k+2) is 0:
            return False
        k += 6
    return True

def process(n, r=10000):
    print("processing: {} ..< {}".format(n, n+r), end="... ")
    s = sum((x for x in range(n, n+r) if is_prime(x) if x <= 2000000))
    print(s)
    return s

def main():
    r = 50000
    with rs4.processing (2) as exe:
        result = 0
        for i in rs4.tqdm (exe.map(rs4.partial(process, r=r), range(0, 2000000, r)), total = 100, color = "yellow"):
            result += i
            print(i, result)
        print(result)

def main2():
    r = 50000
    with rs4.threading (2) as exe:
        fs = [exe.submit(process, n, r) for n in range(0, 2000000, r)]
        done, _ = rs4.waitf (fs)        
        result = sum((f.result() for f in done))
        print(result)
        
if __name__ == "__main__":
    main2()
    