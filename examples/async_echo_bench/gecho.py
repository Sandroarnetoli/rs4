# gevent

from gevent.server import StreamServer

def echo (socket, address):
    print ("Connection from", address)
    while 1:
        data = socket.recv (65535)
        if not data:
            break
        socket.sendall (b'Got:' + data)
    print ('Connection closed')
    socket.close ()    

if __name__ == "__main__":    
    server = StreamServer (("", 25000), echo)
    server.serve_forever ()

    