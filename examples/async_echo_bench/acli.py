from .. import asyncore
from socket import *
import time

class Client(asyncore.dispatcher):
    def __init__(self, address, message, limit = 10):
        self.limit = limit  
        self.message = message      
        self.write_buffer = message
        asyncore.dispatcher.__init__(self)
        self.create_socket(AF_INET, SOCK_STREAM)
        self.connect(address)

    def handle_connect(self):
        pass

    def handle_close(self):
        self.close()

    def writable(self):
        return (len(self.write_buffer) > 0)

    def readable(self):
        return True

    def handle_write(self):
        sent = self.send (self.write_buffer)
        self.limit -= 1
        self.write_buffer = self.write_buffer[sent:]

    def handle_read(self):
        data = self.recv (65535)    
        if data and self.limit:
            self.write_buffer = self.message
            return
        elif not self.limit:
            self.handle_close ()    
        
def main (numcli, msg, limit):
    [Client (("127.0.0.1", 25000), msg, limit // numcli) for i in range (numcli)]
    asyncore.loop ()
    
    
    
if __name__ == '__main__':
    import sys
    import getopt
    import rs4
    
    argopt = getopt.getopt(sys.argv[1:], "c:p:m:", [])
    numcli = 1
    numproc = 1
    msglen = 1
    for k, v in argopt [0]:    
        if k == "-c":
            numcli = int (v)
        elif k == "-p":
            numproc = int (v)
        elif k == "-m":
            msglen = int (v)    
    
    msg = b"x" * msglen             
    limit = int (argopt [1][0])
    start  = time.time ()   
    with rs4.processing (numproc) as exe:
        futures = [exe.submit (main, numcli, msg, limit // numproc) for i in range (numproc)]        
        dones, _ = rs4.waitf (futures)
    end  = time.time ()
    print (limit / (end-start), "messages/sec")
    

"""
100000 times results on Pythoin 3.5 (3 rties) -------------------------------

10 clients, 1 process

 - asyncio 15671, 15836, 15831
 - gevent: 44859, 44338, 45148
 - Beazley hack: 48181, 51059, 48512
 - asyncore: 69425, 75140, 74690 

100 clients, 1 process

 - asyncio: 17143, 17101, 17339
 - gevent: 44230, 44868, 43786
 - Beazley hack: 54153, 50370, 54008
 - asyncore: 102796, 102367, 95060

1000 clients, 1 process

 - asyncio: 16393, 16616, 16165
 - gevent: 40666, 41895, 40666
 - Beazley hack: 22331, 35734, 34086
 - asyncore: 49096, 35935, 22295 

100 clients, 10 process

 - asyncio: 14287, 14489, 13765
 - gevent: 29854, 27925, 31727
 - Beazley hack: 28480, 21960, 12875
 - asyncore: 22461,22693, 22902       
"""

    