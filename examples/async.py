import asyncio
import random
import rs4
import time

async def lazy_greet(msg, delay=1):
  print(msg, "will be displayed in", delay, "seconds")
  await asyncio.sleep(delay)
  return msg.upper()

def lazy_greet2(msg, delay=1):
  print(msg, "will be displayed in", delay, "seconds")
  time.sleep(delay)
  return msg.upper()
  
async def time_log():
  i = 0
  print("time log starts.")
  while True:
    await asyncio.sleep(1)
    i += 1
    print('...%02d sec.' % (i,))

async def main():
  t = rs4.aio.submit (time_log)
  messages = ['hello', 'world', 'apple', 'banana', 'cherry']
  fts = [rs4.aio.submit (lazy_greet, m, random.randrange(1, 5)) for m in messages]
  (done, pending) = await rs4.waitf (fts, timeout=2)
  if pending:
    print("there is {} tasks not completed".format(len(pending)))
    for f in pending:
      f.cancel()
  for f in done:
    x = await f
    print(x)
    
async def main2():
  t = rs4.aio.submit (time_log)
  messages = ['hello', 'world', 'apple', 'banana', 'cherry']
  result = await rs4.aio.map (lazy_greet, messages)
  t.cancel()
  print(result)

async def main3():
  messages = ['hello', 'world', 'apple', 'banana', 'cherry']
  result = await rs4.aio.map (lazy_greet, messages)
  print (result)    
    
rs4.aio.loop (main)    
rs4.aio.loop (main2)
rs4.aio.loop (main3)
